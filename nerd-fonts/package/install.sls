# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as nerd_fonts with context %}

nerd-fonts-package-install-git-pkg-installed:
  pkg.installed:
    - name: {{ nerd_fonts.gitpkg.name }}

nerd-fonts-package-install-repo-cloned:
  git.latest:
    - name: https://github.com/ryanoasis/nerd-fonts.git
    - target: /srv/nerdfonts
    - depth: 1
    - rev: v2.3.3
    - unless: ls /usr/local/share/fonts/NerdFonts
    - require:
      - nerd-fonts-package-install-git-pkg-installed

nerd-fonts-package-install-repo-installed:
  cmd.run:
    - name: /srv/nerdfonts/install.sh --install-to-system-path
    - require:
      - nerd-fonts-package-install-repo-cloned
    - creates: /usr/local/share/fonts/NerdFonts

nerd-fonts-package-install-repo-removed:
  file.absent:
    - name: /srv/nerdfonts
    - require:
      - nerd-fonts-package-install-repo-installed
