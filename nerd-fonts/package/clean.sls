# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as nerd__fonts with context %}

nerd-fonts-package-clean-repo-removed:
  file.absent:
    - name: /usr/local/share/fonts/NerdFonts
