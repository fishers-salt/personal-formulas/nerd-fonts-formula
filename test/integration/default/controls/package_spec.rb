# frozen_string_literal: true

control 'nerd-fonts-package-install-git-pkg-installed' do
  title 'The required package should be installed'

  describe package('git') do
    it { should be_installed }
  end
end

control 'nerd-fonts-package-instsall-repo-installed' do
  title 'fonts should be installed'

  describe directory('/usr/local/share/fonts/NerdFonts') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0755' }
  end
end

control 'nerd-fonts-package-install-repo-removed' do
  title 'should be absent'

  describe directory('/srv/nerdfonts') do
    it { should_not exist }
  end
end
