# frozen_string_literal: true

control 'nerd-fonts-package-clean-repo-removed' do
  title 'should be absent'

  describe directory('/srv/nerdfonts') do
    it { should_not exist }
  end
end

control 'nerd-fonts-package-clean-repo-removed' do
  title 'fonts should not be installed'

  describe directory('/usr/local/share/fonts/NerdFonts') do
    it { should_not exist }
  end
end
